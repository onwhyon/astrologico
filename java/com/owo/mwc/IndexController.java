package com.owo.mwc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.Tools;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.mwc.parsed.*;
import com.owo.web.http.Params;


@Controller
@PropertySource("classpath:/META-INF/cats.properties")
public class IndexController extends VidasaludabeController {
	@Value("${cat.menu}")
	private long catMenu;// = 28;
	@Value("${cat.destacada}")
	private long catDestacda;// = 29;
	@Value("${cat.promotion1}")
	private long catPromo1;// = 30;
	@Value("${cat.promotion2}")
	private long catPromo2;// = 31;
	@Value("${cat.poll}")
	private long catPoll;// = 32;
	@Value("${cat.notification}")
	private long catNotif;// = 39;

	//You need this
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
       return new PropertySourcesPlaceholderConfigurer();
    }

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	MainSlider mainslider;
	@Autowired
	TopText toptext;
	@Autowired
	Article2ColOS article2colos;
	@Autowired
	Banner banner;
	@Autowired
	Article2Col article2col;
	@Autowired
	PollParser poll;
	@Autowired
	Title title;

	
	@RequestMapping(value = {"/","/index.htm*"}, method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request, HttpSession ses) {

		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;
		
		if (ows.getUser().getName().equals(Constants.UNKNOWN) && ses.getAttribute("homeId")==null 
		&& ows.getSite().getOperator().getIdOperator()==Constants.ID_MOVISTAR){
			ses.setAttribute("homeId", true);
			model.addAttribute("url", "http://"+ows.getSite().getUrlBase());
			return gama+"omuid";
		}else if (ses.getAttribute("homeId")!=null && ses.getAttribute("secondTry")==null){
			ses.setAttribute("secondTry", true);
			return  "forward:/svsession/" + Tools.getUrl(request, true);
		}
		
		headermenu.setCategory(cMenu, listMenu);
		headermenu.getParsed(model, param);

		model.addAttribute("subscribed", subscrito);
		model.addAttribute("wifi", wifi);
		model.addAttribute("comisionado", comisionado);

		article2col.paramCat();
		article2col.setCategory(cMenu, listMenu);
		article2col.getParsed(model, param);
 
		Category catOS = catdao.getCategory(catDestacda);
		List<Content> lstOS = cntdao.getContentCategoryListDestacados(catDestacda, 0, 3, true);
		mainslider.paramCnt();
		mainslider.setContent(lstOS);
		mainslider.getParsed(model, param, "_1");

		/* Banner que sustituye al main slider en la gama legacy
		 *  */
		try {
			Content cntdos = lstOS.get(0);
			banner.paramCnt();
			banner.setContent(cntdos);
			banner.getParsed(model, param, "_1");
		} catch (Exception e) {
			// TODO tracear
		}

		toptext.paramCat();
		toptext.setCategory(catOS);
		toptext.getParsed(model, param);

		lstOS = cntdao.getContentCategoryListDestacados(catDestacda, 3, 8,true);
		article2colos.paramCnt();
		article2colos.setContent(catOS, lstOS);
		article2colos.getParsed(model, param, "_1");

		Category catBan = catdao.getCategory(catPromo1);
		banner.paramCat();
		banner.setCategory(catBan);
		banner.getParsed(model, param, "_1");
		catBan = catdao.getCategory(catPromo2);

		banner.setCategory(catBan);
		banner.getParsed(model, param, "_2");

		title.paramCat();
		param.setParam(Params.TITLE1ROWCA, String.format("%d", catNotif));
		title.getParsed(model, param, "_1");

		model.addAttribute("param", param);
		ses.setAttribute("param", param);

		return gama + "index";
	}
}