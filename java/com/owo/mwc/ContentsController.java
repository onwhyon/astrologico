package com.owo.mwc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.mwc.parsed.Article2Col;
import com.owo.mwc.parsed.HeaderMenu;
import com.owo.mwc.parsed.Title;


@Controller
public class ContentsController extends VidasaludabeController{

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	Title title;
	@Autowired
	Article2Col article2col;

	@RequestMapping(value={"/contents.htm*", "/contents/*"})
	public String content(Model model, HttpSession ses, HttpServletRequest request,
			@RequestParam(value="idcat", required=false) String sidcat,
			@RequestParam(value="pag", required=false) String page) {

		String PAGALIAS = Constants.CNTSALIAS;
		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;

		try {

			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);

			model.addAttribute("subscribed", subscrito);

			// Buscamos la categor�a a mostar
			// Chequear si hay algo del estilo /categoria/ en la url por que detr�s vendr� la amigable
			long idcat = 0;
			Category cat = null;
			int currentPage=0;
			String pagination="";
			String url = request.getRequestURI();
			if(url.contains(PAGALIAS)) {
				String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length());
				cat = catdao.getCatFromFriendUrl(friendurl, ows.getIdlanguaje(), catMenu);
				if (cat!=null)
					idcat=cat.getIdCategory();
				pagination = request.getRequestURL().toString()+"?pag=";
			}
			if (cat == null || cat.getIdCategory() <= 0) {
				idcat = Long.parseLong(sidcat);				
				cat = catdao.getCategory(idcat);
				pagination = request.getRequestURL().toString()+"?&idcat="+idcat+"&pag=";
			}
			
			if (page!=null){
				currentPage=Integer.parseInt(page);
			}
			idcat = cat.getIdCategory();
			title.setCategory(cat);
			title.getParsed(model, param);
			
			List<Content> arrcnts = cntdao.getContentCategoryList(idcat, currentPage*limitepags, limitepags);
			ses.setAttribute("lastIdCat", sidcat);
			paginacionCnts(model, idcat, currentPage);
			article2col.setContent(cat, arrcnts);
			article2col.getParsed(model, param);

			model.addAttribute("prvpage", pagination+(currentPage-1));
			model.addAttribute("nxtpage", pagination+(currentPage+1));
			
			sRet = gama +"contents";
		} catch (NumberFormatException nfe) {
			
		}

		return sRet;
	}
}
