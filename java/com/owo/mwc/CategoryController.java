package com.owo.mwc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.mwc.parsed.Article2Col;
import com.owo.mwc.parsed.Article2ColOS;
import com.owo.mwc.parsed.Banner;
import com.owo.mwc.parsed.HeaderMenu;
import com.owo.mwc.parsed.MainSlider;
import com.owo.mwc.parsed.Title;
import com.owo.mwc.parsed.TopText;

@Controller
public class CategoryController extends VidasaludabeController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	MainSlider mainslider;
	@Autowired
	TopText toptext;
	@Autowired
	Article2ColOS article2colos;
	@Autowired
	Title title;
	@Autowired
	Article2Col article2col;
	@Autowired
	Banner banner;

	@RequestMapping(value = {"/category.htm*", "/category/*"}, method = RequestMethod.GET)
	public String category(Model model, HttpSession ses, HttpServletRequest request,
			@RequestParam(value="idcat", required=false) String sidcat) {

		String PAGALIAS = Constants.CATALIAS;
		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;

		try {

			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);

			model.addAttribute("subscribed", subscrito);

			// Buscamos la categor�a a mostar
			// Chequear si hay algo del estilo /categoria/ en la url por que detr�s vendr� la amigable
			long idcat = 0;
			Category cat = null;
			String url = request.getRequestURI();
			if(url.contains(PAGALIAS)) {
				String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length());
				cat = catdao.getCatFromFriendUrl(friendurl, ows.getIdlanguaje(), catMenu);
			} 
			if (cat == null || cat.getIdCategory() <= 0 && request.getParameter("idcat")!=null) {
				idcat = Long.parseLong(request.getParameter("idcat"));				
				cat = catdao.getCategory(idcat);
			}
			idcat = cat.getIdCategory();

			title.setCategory(cat);
			title.getParsed(model, param);

			int ini  = 0;
			boolean legacy = ows.getDevice().getGama().getIdGama() == 4;
			int elems = (legacy)?1:3;
			//List<Content> arrcnts = cntdao.getContentCategoryList(idcat, ini, elems);
			List<Content> arrcnts = cntdao.getLatest(ows.getIdlanguaje(), idcat, ini, elems);
			mainslider.paramCnt();
			mainslider.setContent(arrcnts);
			mainslider.getParsed(model, param);

			/* Banner que sustituye al main slider en la gama legacy */
			try {
				Content cntdos = arrcnts.get(0);
				banner.paramCnt();
				banner.setContent(cntdos);
				banner.getParsed(model, param, "_1");
			} catch (Exception e) {
				//TODO trazas
			}

			ini = (legacy)?1:3;
			elems = (legacy)?4:2;
			//arrcnts = cntdao.getContentCategoryList(idcat, ini, elems);
			arrcnts = cntdao.getLatest(ows.getIdlanguaje(), idcat, ini, elems);
			article2colos.paramCat();
			article2colos.setContent(cat, arrcnts);
			article2colos.getParsed(model, param);

			List<Category>arrcats = catdao.getCategoryList(idcat, 0, 0);
			article2col.paramCat();
			article2col.setCategory(cat, arrcats);
			article2col.getParsed(model, param);

			sRet = gama + "category";
		} catch (NumberFormatException nfe) {

		}

		return sRet;
	}
}
