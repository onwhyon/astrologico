package com.owo.mwc;



import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.Texts;
import com.owo.dao.domain.Tools;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.dao.domain.site.Site;
import com.owo.mwc.parsed.Arti3ColOnlyText;
import com.owo.mwc.parsed.Banner;
import com.owo.mwc.parsed.ContentLongparagraph;
import com.owo.mwc.parsed.HeaderMenu;
import com.owo.mwc.parsed.IntroNews;
import com.owo.mwc.parsed.TaskList;
import com.owo.mwc.parsed.Title;

@Controller
public class ContentController extends AstrologicoController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	Title title;
	@Autowired
	Banner banner;
	@Autowired
	IntroNews intronews;
	@Autowired
	TaskList tasklist;
	@Autowired
	ContentLongparagraph longp;
	@Autowired
	Arti3ColOnlyText articleOnlytext;

	//@RequestMapping(value={"/content.htm*", "/premium/content.htm*", "/premium/content/*/*", "/content/*/*"})
	@RequestMapping(value={"/premium/content.htm*", "/premium/content/*/*"})
	public String content(Model model, HttpSession ses, HttpServletRequest request,
			@RequestParam(value="idcnt", required=false)String sidcnt,
			@RequestParam(value="idcat", required=false)String sidcat) {

		String PAGALIAS = Constants.CNTALIAS;

		String url = Tools.getUrl(request, true);

		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;

		boolean subscrito = (Boolean) ses.getAttribute(Constants.SUSBCRIBED);
		
		if (!subscrito) {
			return "forward:" + url.replace("premium", "billingsbs");
		}
		
		try {
	
			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);

			// Buscamos la categor�a y el contenido a mostar
			// Chequear si hay algo del estilo /categoria/ en la url por que detr�s vendr� la amigable
			long idcat = 0;
			long idcnt = 0;
			Category cat = null;
			Content cnt = null;
			if(url.contains(PAGALIAS)) {
				int posfin = url.length();
				if(url.contains("?"))
					posfin = url.indexOf("?");
				String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length(), posfin);
				cnt = cntdao.getCntFromFriendUrl(friendurl, ows.getIdlanguaje());
			}
			if (StringUtils.isNotEmpty(sidcat) && (cat == null || cat.getIdCategory() <= 0)) {
				idcat = Long.parseLong(sidcat);				
				cat = catdao.getCategory(idcat);
			}
			if (cnt == null || cnt.getIdContent() <= 0) {
				idcnt = Long.parseLong(sidcnt);
				cnt = cntdao.getContent(idcnt);
			}
			idcnt = cnt.getIdContent();

			Set <Category> catref=null;
			Iterator <Category >categories = null;
			if (cnt!=null){
				catref = cnt.getCategories();
				if(catref!=null){
					categories = catref.iterator();
				}
				cat = catdao.getCategory(categories.next().getIdCategory());
				if (cat.getIdCategory()==catDestacda && categories.hasNext()){
					cat = catdao.getCategory(categories.next().getIdCategory());
				}
			}
			if (cat != null){
				idcat = cat.getIdCategory();
			}
			
			title.setContent(cnt);
			title.getParsed(model, param);

			banner.setContent(cnt);
			banner.getParsed(model, param);

			intronews.setContent(cnt);
			intronews.getParsed(model, param);

			tasklist.setContent(cnt);
			tasklist.getParsed(model, param);

			longp.setContent(cnt);
			longp.setParam("bodynews_1");
			longp.getParsed(model, param);

			sRet = gama +"content";

			if(cat != null && idcat > 0) {
				List<Content> arrcnt = cntdao.getContentCategoryList(idcat, 0, 3);
				if (arrcnt != null) {
					for(int i = 0; i < 2 & i < arrcnt.size(); i++) {
						cnt = arrcnt.get(i);
						if(cnt.getIdContent()==idcnt) {
							arrcnt.remove(i);
						}
					}
					// Nos aseguramos que s�lo sean dos los contenidos
					if(arrcnt.size() > 2)
						arrcnt.remove(2);
					
					articleOnlytext.setContent(cat, arrcnt);
					articleOnlytext.getParsed(model, param);
				}
			}
		} catch (NumberFormatException nfe) {

		}

		List<Content> arrcntRec = cntdao.getContentCategoryList(catRecomended, 0, 2);
		Category cat = catdao.getCategory(catRecomended);
		if (arrcntRec!=null && cat!=null){
			Set <Site> sites = cat.getSites();
			Site site = null;
			if (sites!=null)
				site = sites.iterator().next();
			for (Content cnt : arrcntRec){
				long idcont = cnt.getIdContent();
				Texts t = null;
				t = textdao.getCntTextByType(idcont, 20);
				if (t==null)
					t = textdao.getCntTextByType(idcont, 15);
				if (t!=null){
					
					Set <Texts> txts = cnt.getTexts();
					for (Texts txt : txts){
						if (txt.getIdTextType()==15 && t.getIdTextType()==15){
							t.setText(site.getUrlBase()+"/premium/" + t.getText());
							txts.remove(txt);
							txts.add(t);
							break;
						}else 
							if (txt.getIdTextType()==20 && t.getIdTextType()==20){
								t.setText(site.getUrlBase()+"/premium/content/" + t.getText());
								txts.remove(txt);
								txts.add(t);
								break;
							}
					}
				}
			}
			articleOnlytext.setContent(cat, arrcntRec);
			articleOnlytext.getParsed(model, param, "_1");
			
			model.addAttribute("comisionado", comisionado);

		}
		
		return sRet;
	}
}
