<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--Declare page as mobile friendly --> 
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
	<!-- Declare page as iDevice WebApp friendly -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	
	<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114"
		href="${pageContext.request.contextPath}/resources/images/splash/splash-icon.png">

	<%-- Si hay que comentar estos tres de debajo --%>
	<!-- iPhone 3, 3Gs -->
	<link rel="apple-touch-startup-image"
		href="${pageContext.request.contextPath}/resources/images/splash/splash-screen.png"
		media="screen and (max-device-width: 320px)" /> 
	<!-- iPhone 4 -->
	<link rel="apple-touch-startup-image"
		href="${pageContext.request.contextPath}/resources/images/splash/splash-screen@2x.png"
		media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
	<!-- iPhone 5 -->
	<link rel="apple-touch-startup-image" sizes="640x1096"
		href="${pageContext.request.contextPath}/resources/images/splash/splash-screen@3x.png" />

	<title>Astrologico</title>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/scripts/bxslider_custom_snap.js" type="text/javascript"></script>
	
	<link href="${pageContext.request.contextPath}/resources/styles/vidahtml5.css" rel="stylesheet" type="text/css">
</head>