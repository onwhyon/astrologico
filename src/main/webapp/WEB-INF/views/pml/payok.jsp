﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headPml.jsp" %>
	<body>	
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"  />
		
			<table class="container">
			<tr>
			<td>
			<p style="text-align: center; margin-top: 12px;" class="mod-intro"><span><b>Suscripción realizada con éxito disfruta a 0,00€ de todo el contenido la primera semana (si es la primera vez que accedes), el resto de semanas a <%=EUR %>€ IVA incl. </b> </br>Haz click en el enlace para acceder al artículo. </span></p>
			</td>
			</tr>
			
			<tr>
			<td>
			<h2 style="text-align: center;" ><a href="${pageContext.request.contextPath}/${link} " class="botonGrey"  >ACCEDER AL ARTÍCULO</a></h2>
			</td>
			</tr>
			<tr style="text-align: left;">
			<td>
			<p>A partir de ahora puedes acceder a todos los artículos que más te gusten gracias a tu suscripción autorenovable semanal. Para volver a acceder al servicio recuerda entrar en el portal <%=SITE %> dentro de Emoción.</p>
			</td>
			</tr>
			<tr style="text-align: left;">
			<td>
			<p>Si has contratado una suscripción y deseas gestionarla o darte de baja, puedes hacerlo desde la sección <a class="link" href="http://wap.movistar.com/primary/MisSuscripciones">"Mis suscripciones"</a> de Emoción haciendo <a class="link" href="http://wap.movistar.com/primary/MisSuscripciones">click aquí</a>. Te recordamos que desde
            		<a class="link" href="http://wap.movistar.com/primary/MisSuscripciones">"Mis suscripciones"</a> puedes acceder a todos los servicios a los que estés suscrito. Además, puedes resolver cualquier problema en el proceso de suscripción, compra y su visualización llamando gratis
            		al 1004.
            		
            	</p>
            </td>
            </tr>
            <tr style="text-align: left;">
            <td>	
			<p>El importe de la compra o suscripción será cargado en tu próxima factura Movistar o se descontara de tu tarjeta prepago dentro del apartado acceso a contenidos.</p>
			</td>
			</tr>
			</table>
		

			<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
		
	</body>
</html>