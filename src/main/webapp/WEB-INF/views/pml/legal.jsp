﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headPml.jsp" %>
	<body>	
		
		<body>	
		
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"  />
		<owo:menu list="${headermenucats }" pag="<%=PRM + CAT %>"/>
		<owo:title1row title="${legaltitle }"/>
        
    		<table style="text-align: left;">
	    		<tr>
		    		<td>
		    			<c:if test="${gestion=='false'}">
			        		<p ALIGN="CENTER"><strong>Condiciones particulares del Servicio</strong></p>
<p ALIGN="CENTER"> <strong>CONDICIONES PARTICULARES DEL SERVICIO DE COMPRA DE CONTENIDOS EN EMOCION </strong></p>
<p ALIGN="CENTER"><BR>
</p>
<p ALIGN="JUSTIFY"> <strong>1.- CONDICIONES DEL SERVICIO Y SU ACEPTACIÓN</strong></p>
<p ALIGN="JUSTIFY"> Las Condiciones descritas a continuación (en adelante, las &quot;Condiciones Generales&quot;) regulan las relaciones contractuales entre TELEFÓNICA MÓVILES ESPAÑA, S.A.U. (en adelante &quot;MOVISTAR&quot;), con CIF nº A-78923125., domiciliada en Distrito C, Edificio Sur 3, Ronda de la Comunicación s/n, 28050 Madrid, inscrita en el Registro Mercantil de Madrid, ST-1, Tomo 8958, General 7804, de la Sección 3ª del LIBRO DE SOCIEDADES, Folio 92, Hoja nº 85226-1, Inscripción 1ª - y el Cliente Movistar (en adelante el &quot;Cliente&quot;) en todo lo relativo a la facilidad de compra de contenidos desde la web móvil <U>wap.movistar.com</U> (en adelante &quot;el Servicio&quot;). </p>
<p ALIGN="JUSTIFY">La aceptación, sin reservas, de las presentes Condiciones es indispensable para la prestación del Servicio por parte de MOVISTAR. Al aceptar las presentes condiciones, el cliente consiente que se inicie la ejecución del  suministro de este contenido digital, con el conocimiento por su parte de que en consecuencia pierde su derecho de desistimiento. El Cliente manifiesta, en tal sentido, haber leído, entendido y aceptado las presentes Condiciones, puestas a su disposición, en todo momento, con carácter previo a la compra, en la siguiente página web móvil : <U><A HREF="http://www.emocion.movistar.es/">wap.movistar.com</A></U>. El servicio objeto de las presentes Condiciones se prestará conforme a lo previsto en las mismas, y en todo lo no modificado expresamente, por las condiciones generales de prestación del servicio Movistar móvil, publicadas en la página web de MOVISTAR, que el cliente declara expresamente conocer y aceptar. Así mismo la utilización del Servicio conlleva, asimismo, la aceptación por parte del Cliente de cuantos avisos, reglamentos de uso e instrucciones fueren puestos en su conocimiento por parte de MOVISTAR con posterioridad a la aceptación de las presentes Condiciones Generales; su no aceptación, dentro del plazo al efecto otorgado, conllevará la terminación del Contrato por parte del Cliente. </p>
<p ALIGN="JUSTIFY"> <strong>2. OBJETO Y PROCEDIMIENTO DE COMPRA</strong></p>
<p ALIGN="JUSTIFY"> Las presentes Condiciones regulan la facilitad de compra por el Cliente de contenidos publicados por MOVISTAR en la página <U><A HREF="http://www.emocion.movistar.es/">wap.movistar.com</A></U>.&nbsp;&nbsp; A través de esta página de internet móvil, el cliente puede acceder al catálogo de contenidos de emoción (juegos, música, video, imágenes, noticias, deportes y otros contenidos de entretenimiento para el móvil).</p>
<p ALIGN="JUSTIFY"> <strong>2.1. Cobro de la compra </strong></p>
<p ALIGN="JUSTIFY"> El Cliente, una vez que ha seleccionado el contenido disponible del catálogo de emoción debe pulsar el botón o enlace  que supone asunción de un cargo (p.e &quot;Comprar&quot;, &ldquo;Suscribir&rdquo;). Al pulsar en estos botones o enlaces se habrá confirmado la compra y el pago, iniciándose el proceso de descarga si el contenido es descargable en el acto. Si el contenido se ofrece bajo un modelo de suscripción, el Cliente adicionalmente recibirá en breves instantes un mensaje de texto en su móvil con información sobre contenido al que se suscribe, y un enlace para acceder al contenido, estas condiciones, y a la gestión de la suscripción. Si se pulsa en este enlace se facturará el coste de navegación móvil o datos según plan de tarifas del cliente. El cobro de la compra del contenido se realiza en la propia factura del cliente en el caso de los servicios con contrato o se descuenta del saldo disponible en la tarjeta prepago. Para contenidos de pago por descarga única, el  cobro del contenido se hace en el momento de la compra. Para contenidos de pago bajo un modelo de suscripción temporal (por ejemplo de X euros a la semana), el cobro se hará tanto en el momento de la primera compra, como en el comienzo de cada periodo de renovación de la suscripción</p>
<p ALIGN="JUSTIFY"> <BR>
  <BR>
</p>
<p> <em>Los contenidos de este site se ofrecen en modalidad de Suscripción auto-renovable a un precio de <%=EUR %>€/semana IVA incl. Esta suscripción le da derecho a visualizar todos los contenidos que desee en los 7 días que tiene la semana.</em></p>
<p ALIGN="JUSTIFY"> <BR>
  <BR>
</p>
<p ALIGN="JUSTIFY"> <strong>2.2. </strong><strong>Modificación y Cancelación del Servicio</strong></p>
<p ALIGN="JUSTIFY">Si has contratado una suscripción y deseas gestionarla o darte de baja, puedes hacerlo desde la sección <U><A HREF="http://wap.movistar.com/primary/MisSuscripciones">Mis Suscripciones</A></U> de emoción haciendo <U><A HREF="http://wap.movistar.com/primary/MisSuscripciones">clic aquí</A></U><em><U>.</U></em><em> </em><em>.</em> Te recordamos que desde Mis Suscripciones puedes acceder a todos los servicios a los que estás suscrito.</p>
<p ALIGN="JUSTIFY">Además, también puedes resolver cualquier problema en el proceso de suscripción, compra y visualización llamando gratis al 1004.</p>
<p><BR>
</p>
<p ALIGN="JUSTIFY"><BR>
</p>
<p ALIGN="JUSTIFY">MOVISTAR podrá modificar las presentes Condiciones Particulares por los siguientes motivos: variaciones de las características técnicas de los equipos o las redes, cambios tecnológicos que afecten al Servicio, variaciones de las condiciones económicas existentes en el momento de la contratación del Servicio y evolución del mercado, comunicándoselo al cliente con un (1) mes de antelación a la fecha en que la modificación vaya a ser efectiva. </p>
<p ALIGN="JUSTIFY"><BR>
</p>
<p ALIGN="JUSTIFY">En dichos supuestos, el Cliente tendrá derecho a resolver la relación de prestación de servicios regulada en las presentes Condiciones Particulares sin penalización alguna, sin perjuicio de otros compromisos adquiridos por el propio Cliente.</p>
<p ALIGN="JUSTIFY"><BR>
</p>
<p ALIGN="JUSTIFY">Las facultades de modificación de MOVISTAR no perjudican el derecho de resolución anticipada del contrato reconocido al Cliente en las Condiciones Generales de prestación del servicio de comunicaciones móviles Movistar, sin penalización alguna por este motivo, sin perjuicio de otros compromisos adquiridos por el Cliente.</p>
<p ALIGN="CENTER"><BR>
</p>
<p ALIGN="JUSTIFY"> <strong>3. OBLIGACIONES DEL CLIENTE</strong></p>
<p ALIGN="JUSTIFY"> <strong>3.1. Precio</strong></p>
<p ALIGN="JUSTIFY"> El uso del Servicio no conlleva coste adicional alguno para el Cliente, salvo el de la navegación móvil para llegar al contenido, y deberá abonar únicamente el importe de la compra del contenido emoción que desee adquirir, así como el coste de la navegación asociado a la descarga. Los precios de cada uno de los contenidos se publicitan en la web móvil de emoción antes de cada compra. </p>
<p ALIGN="JUSTIFY"> <strong>3.2. Uso correcto del Servicio y Contenidos</strong></p>
<p ALIGN="JUSTIFY"> El Cliente se compromete a utilizar el Servicio y los Contenidos de conformidad con la ley, la moral y buenas costumbres generalmente aceptadas y el orden público, así como a abstenerse de utilizar el Servicio con fines o efectos ilícitos, prohibidos en la presentes Condiciones Generales, lesivos de los derechos e intereses de terceros, o que de cualquier forma puedan dañar, inutilizar, sobrecargar o deteriorar el Servicio. En particular, el Cliente se compromete a abstenerse de (a) reproducir o copiar, distribuir, permitir el acceso del público a través de cualquier modalidad de comunicación pública, transformar o modificar los Contenidos, a menos que se cuente con la autorización del titular de los correspondientes derechos o ello resulte legalmente permitido; (b) suprimir, eludir o manipular el &quot;copyright&quot; y demás datos identificativos de los derechos de los titulares incorporados a los Contenidos, así como los dispositivos técnicos de protección, las huellas digitales o cualesquiera mecanismos de información que pudieren contener los Contenidos. Los Clientes perjudicados por dichas prácticas podrán comunicarlo a MOVISTAR a través del formulario que se habilita en <U><A HREF="http://www.movistar.es/">www.movistar.es</A></U>. MOVISTAR se reserva la facultad de adoptar las medidas que estime oportunas en caso de que existan indicios de un uso fraudulento del Servicio, incluyendo la adopción de las acciones legales oportunas, sin perjuicio de las medidas que correspondan a las distintas operadoras. </p>
<p ALIGN="JUSTIFY"> <strong>3.3. Responsabilidad por daños y perjuicios</strong></p>
<p ALIGN="JUSTIFY"> El Cliente responderá de los daños y perjuicios de toda naturaleza que MOVISTAR pueda sufrir como consecuencia del incumplimiento de cualquiera de las obligaciones a las que queda sometido por virtud de las Condiciones Generales o de la ley en relación con la utilización del Servicio. </p>
<p ALIGN="JUSTIFY"> <strong>3.4. - Medios para la obtención de los contenidos</strong></p>
<p ALIGN="JUSTIFY"> El Cliente se compromete a abstenerse de obtener e incluso de intentar obtener los Contenidos que resulten accesibles a través del Servicio del portal empleando para ello medios o procedimientos distintos de los que, en cada caso, se hayan proporcionado o se hayan indicado a tal efecto. </p>
<p ALIGN="JUSTIFY"> <strong>4. DATOS DE CARÁCTER PERSONAL</strong></p>
<p ALIGN="JUSTIFY"> El tratamiento de los datos aportados al registrarse en el servidor de Telefónica Móviles España, S.A.U. en los términos señalados en la condición segunda, junto con los generados en la compra de contenidos a través de la web móvil de emoción, se harán cumpliendo escrupulosamente la legislación vigente en materia de protección de datos. </p>
<p ALIGN="JUSTIFY"> En cumplimiento de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, se informa que los datos de carácter personal facilitados por el usuario en el registro junto con los del tráfico generado para realizar la compra, serán incorporados a un fichero titularidad de Telefónica Móviles España S.A.U., responsable del tratamiento y destinataria de los datos e información, ante la cual podrán ejercitar los derechos de acceso, rectificación, cancelación y oposición previstos en la Ley, mediante escrito dirigido a apartados de correos 56  48080, Bilbao, Referencia DATOS. </p>
<p ALIGN="JUSTIFY"> <strong>5. OBLIGACIONES DE MOVISTAR</strong></p>
<p ALIGN="JUSTIFY"> <strong>5.1. Disponibilidad</strong></p>
<p ALIGN="JUSTIFY"> El Servicio estará disponible de forma permanente, veinticuatro (24) horas al día. MOVISTAR no garantiza la disponibilidad, continuidad ni, en general, normal funcionamiento del Servicio en el caso de incidencias e interrupciones en el normal funcionamiento de las redes involucradas en el Servicio y, en particular, en la red Internet.</p>
<p ALIGN="JUSTIFY"> <strong>5.2.- Información al cliente.</strong></p>
<p ALIGN="JUSTIFY"> MOVISTAR facilitará el contenido de las presentes condiciones en su página de Internet, por escrito si así lo solicita EL Cliente en las Tiendas de distribución de MOVISTAR. Los canales habitados por MOVISTAR para atender e informar a los clientes serán los siguientes: por internet en <U><A HREF="http://www.atencionenlinea.movistar.es/">www.atencionenlinea.movistar.es</A></U> , y por teléfono en el 1004 si llama desde su Movistar (1489 si llama desde cualquier otro teléfono). </p>
<p ALIGN="JUSTIFY"> <strong>6. EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD</strong></p>
<p ALIGN="JUSTIFY"> <strong>6.1. Disponibilidad, continuidad, utilidad y falibilidad</strong></p>
<p ALIGN="JUSTIFY"> MOVISTAR no garantiza la disponibilidad y continuidad del funcionamiento del Servicio. Cuando ello sea razonablemente posible, MOVISTAR advertirá previamente las interrupciones en el funcionamiento del Servicio. MOVISTAR tampoco garantiza la utilidad del Servicio para la realización de ninguna actividad en particular, ni su infalibilidad y, en particular, aunque no de modo exclusivo, que los Clientes puedan efectivamente acceder a las distintas páginas de web móvil que forman el Servicio ni que a través del mismo se puedan difundir o poner a disposición de terceros informaciones, datos o contenidos o acceder a informaciones, datos o contenidos difundidos o puestos a disposición por terceros. MOVISTAR EXCLUYE CUALQUIER RESPONSABILIDAD POR LOS DAÑOS Y PERJUICIOS DE TODA NATURALEZA QUE PUEDAN DEBERSE A LA FALTA DE DISPONIBILIDAD O DE CONTINUIDAD DEL FUNCIONAMIENTO DEL SERVICIO, A LA DEFRAUDACIÓN DE LA UTILIDAD QUE LOS CLIENTES HUBIEREN PODIDO ATRIBUIR AL SERVICIO Y A LOS FALLOS DEL SERVICIO Y, EN PARTICULAR, AUNQUE NO DE MODO EXCLUSIVO, A LOS FALLOS EN EL ACCESO A LAS DISTINTAS PÁGINAS WEB DEL SERVICIO, ASÍ COMO A LOS FALLOS EN LA DIFUSIÓN, PUESTA A DISPOSICIÓN O ACCESO A INFORMACIONES, DATOS O CONTENIDOS. </p>
<p ALIGN="JUSTIFY"> <strong>6.2. Privacidad y seguridad en la utilización del Servicio</strong></p>
<p ALIGN="JUSTIFY"> MOVISTAR no garantiza la privacidad y la seguridad en la utilización de los servicios por parte de los CLIENTES y, en particular, no garantiza que terceros no autorizados no puedan tener conocimiento de la clase, condiciones, características y circunstancias del uso de Internet que puedan hacer los CLIENTES o que no puedan acceder y, en su caso, interceptar, eliminar, alterar, modificar o manipular de cualquier modo los contenidos y comunicaciones de toda clase que los Clientes transmitan, difundan, almacenen, pongan a disposición, reciban, obtengan o accedan a través del Servicio. </p>
<p ALIGN="JUSTIFY"> MOVISTAR EXCLUYE TODA RESPONSABILIDAD CON TODA LA EXTENSIÓN QUE PERMITA EL ORDENAMIENTO JURIDICO, POR LOS DAÑOS Y PERJUICIOS DE CUALQUIER NATURALEZA QUE PUEDAN DEBERSE AL CONOCIMIENTO QUE PUEDAN TENER LOS TERCEROS DE LA CLASE, CONDICIONES, CARACTERÍSTICAS Y CIRCUNSTANCIAS DEL USO DE INTERNET QUE PUEDAN HACER LOS USUARIOS O QUE PUEDAN DEBERSE AL ACCESO Y, EN SU CASO, A LA INTERCEPTACIÓN, ELIMINACIÓN, ALTERACIÓN, MODIFICACIÓN O MANIPULACIÓN DE CUALQUIER MODO DE LOS CONTENIDOS Y COMUNICACIONES DE TODA CLASE QUE LOS CLIENTES O USUARIOS TRANSMITAN, DIFUNDAN, ALMACENEN, PONGAN A DISPOSICIÓN, RECIBAN, OBTENGAN O ACCEDAN A TRAVÉS DEL SERVICIO. </p>
<p ALIGN="JUSTIFY"> <strong>ANEXO I</strong></p>
<p ALIGN="JUSTIFY"> El precio de los diferentes servicios ofertados en la web móvil <U><A HREF="http://www.emocion.movistar.es/">wap.movistar.com</A></U> se compone del  precio de la descarga o del precio de la suscripción de los diferentes servicios y un precio de navegación móvil hasta llegar al contenido o en su descarga, que variará en función del plan de tarifas del cliente. </p>
<p ALIGN="CENTER"> <strong>Precio de la descarga o suscripción de los diferentes servicios:</strong></p>
<UL>
  <LI>
    <p ALIGN="JUSTIFY"> Juegos:     Entre 0.99 euros IVA incluido) y 18,14 euros IVA incluido)</p>
  <LI>
    <p ALIGN="JUSTIFY"> Imágenes     y Videos: (Fondos de pantalla, Animaciones, Temas, Videos): Entre     0.99 euros IVA incluido) y 2,42 euros IVA incluido)</p>
  <LI>
    <p ALIGN="JUSTIFY"> Canciones     Mp3 y Tonos: Entre 0.99 euros IVA incluido y  7,25 euros IVA     incluido)</p>
  <LI>
    <p ALIGN="JUSTIFY"> Noticias     y Deportes: Entre 0.99 IVA incluido y 3,62 IVA incluido)</p>
</UL>
<p ALIGN="JUSTIFY">Los Precios indicados incluyen el IVA aplicado en Península y Baleares (21%). A los anteriores precios sin IVA le serán de aplicación en Ceuta IPSI 10%, en Melilla IPSI 4% y en Canarias IGIC 7%. </p>
<p ALIGN="JUSTIFY"> Además de estos precios base, en todos estos servicios podrán existir promociones especiales o precios exclusivos de duración determinada, que serán distintos de los anteriores. Estos precios especiales serán notificados explícitamente al cliente antes de prestar su consentimiento para la descarga en su terminal móvil, tanto en el entorno de la página web, como en el entorno de la página web móvil que aloja el contenido concreto. </p>

</c:if>
<c:if test="${gestion=='true'}">
		        		<p>Si has contratado una suscripción y deseas gestionarla o darte de baja, puedes hacerlo desde la sección <a class="link" href="http://wap.movistar.com/primary/MisSuscripciones">"Mis suscripciones"</a> de Emoción haciendo <a class="link" href="http://wap.movistar.com/primary/MisSuscripciones">click aquí</a>. Te recordamos que desde
		            		<a class="link" href="http://wap.movistar.com/primary/MisSuscripciones">"Mis suscripciones"</a> puedes acceder a todos los servicios a los que estés suscrito. Además, puedes resolver cualquier problema en el proceso de suscripción, compra y su visualización llamando gratis
		            		al 1004.
		            		<br><br>
		            	</p>
</c:if>
		            	</td>
	            	</tr>
    		</table>
			

			<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
		
	</body>
		
	</body>
</html>