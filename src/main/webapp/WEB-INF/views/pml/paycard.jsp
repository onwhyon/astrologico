﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headPml.jsp" %>
	<body>
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }" />
		<owo:article1row list="${article1col_1 }" />

		<table>
			<tr>
				<td>
					<p> Activa <%=SITE %> y consigue la mejor revista digital <%=DESC %></p>
					<c:if test="${COMPLETA }">
						<a href="${bannerlnk }"><img src="${pageContext.request.contextPath}/resources/images/misc/pago.gif" class="week"/></a>
					</c:if>
				</td>
			</tr>
			<tr>
				<td>
					<p style="text-align: center;">
						<b>Suscr&iacute;bete por </b><span><b><%=EUR%></b></span><b>&euro;/semana IVA incl.</b>
						<c:if test="${COMPLETA }"><br/> Disfruta a <b>0,00€</b> de todo el contenido la primera semana</c:if>
					</p>
				</td>
			</tr>
		</table>
	
		<table class="encuesta-butt-mod">
			<tr>
				<td class="tableColumn">
				<a href="/index.htm" class="botonRed" onclick="ga('send', 'event','acciones_carta_de_pago', 'cancelar_suscripcion');">Cancelar</a>
				<td class="tableColumn">
				<a href="${subscribe }" class="botonGreen" onclick="ga('send', 'event','acciones_carta_de_pago', 'aceptar_suscripcion');"> Activar </a>
			</tr>
		</table>
	
		<table>
			<tr>
				<td>
					<p>Pulsa en "Activar" para suscribirte al servicio
						<c:if test="${COMPLETA }">
							 y disfrutar de la primera semana gratis.
							 <br/>Pasado este periodo el importe será cargado en tu tarjeta prepago o factura Movistar.
						</c:if>
						<c:if test="${!COMPLETA }">
							.<br/>El importe será cargado en tu tarjeta prepago o factura Movistar.
						</c:if>
					</p>
			<br><p style="text-align: center;"><strong><a href="${pageContext.request.contextPath}/condicionesservicio.htm"></a></strong></p><br>
				</td>
			</tr>
		</table>
	
	
		<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE %>" />
			<script type="text/javascript">
						ga('send', 'event', 'cartadepago',
								'carta_pago_mostrada');
					</script>
	</body>
</html>