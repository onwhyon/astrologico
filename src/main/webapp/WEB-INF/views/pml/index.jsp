<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
<%@ include file="../inc/headPml.jsp" %>
	
	<body>
		   <owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"/>
		   <owo:menu list="${headermenucats }" pag="<%=CNTS %>"/>
		   <owo:title1row title="Categorias:" />
		   <owo:article2col list="${article2col_0}" pag="<%=CNTS %>"/>

	</body>
</html>
