<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<%@ include file="../inc/headPml.jsp" %>
	
	<body>
	   <owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"/>
	   <owo:menu list="${headermenucats }" pag="<%=PRM + CAT %>"/>
	   <owo:title1row title="${title1row_1_002 }" />
	   <owo:banner rect="${banner_1_012 }" image="${banner_1_101 }" imgalt="${banner_1_016 }" />
	   <span class="alinearIzq">
		   <owo:introNews text="${intronews_1_005}"/>
		   
		   <owo:taskList list="${tasklist_1_008 }" title="${tasklist_1_007 }" />
		   <owo:bodyNews text="${bodynews_1_006}"/>
		   <owo:article1Rec list="${article3colonlytext_1_1 }" title="Te recomendamos..." />
		   <owo:article1rel list="${article3colonlytext_1 }" title="${article3colonlytext_1_018}" pag="<%=PRM + CNT %>"/>
		
		
	  </span>
	  <owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
 </body>
</html>