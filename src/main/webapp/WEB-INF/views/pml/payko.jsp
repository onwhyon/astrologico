﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<%@ include file="../inc/headPml.jsp" %>
	<body>	
	
		<owo:headerMenu image="images/logo.gif" imageAlt="VidaSaludable" />
			
			<owo:title1row title="Ooops, lo sentimos" />
   		<table >
   		<tr>
   		<td>
        	<p> ${errTxt }</p>
        	</td>
        	</tr>
   		</table>
		

			<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
		  <script type="text/javascript"> ga('send', 'event', 'error_comunicacion_movistar', 'error_movistar');</script>
	</body>
</html>