<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/pml" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<%@ include file="../inc/headPml.jsp" %>
	<body>
				<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"/>
				<owo:menu list="${headermenucats }" pag="<%=PRM + CAT %>" />
				<p style="text-align: center;">${toptext }</p>

				<owo:title1row title="${title1row_0_002 }" />
				<owo:article1rowCnts list="${article2col_1}" title="${article1col_1_002}" pag="<%=PRM + CNT %>"/>
								   		 <c:if test="${prev==true || sig==true }">
			   <table class="encuesta-butt-mod"><tr>
			 </c:if>
			 <c:if test="${prev }">
		                <td class="tablecolumn"><a class="pag-button" href="${prvpage}" >Anterior</a></td>
	   		 </c:if>
	   		 <c:if test="${sig }">	
	   		 	<td class="tablecolumn"><a class="pag-button" href="${nxtpage}" >Siguiente</a></td>
	   		 </c:if>	
	   		  <c:if test="${prev==true || sig==true }">
			   </tr></table>
			 </c:if>
  
				<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
	</body>
</html>