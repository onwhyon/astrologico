﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/touch" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headHtml5.jsp" %>
	<body>
		<owo:header image="images/logo.gif" imageAlt="VidaSaludable" />
			<owo:title1row title="Ooops, lo sentimos" />
			<div class="paycontainer">
			<p style="text-align: center;" class="mod-intro">${errTxt}</p>

			<owo:footerLegalMovistar />

			<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
			<script type="text/javascript"> ga('send', 'event', 'error_comunicacion_movistar', 'error_movistar');</script>
		</div>
	</body>
</html>