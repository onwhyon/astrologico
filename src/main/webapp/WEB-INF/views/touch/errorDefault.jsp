<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib tagdir="/WEB-INF/tags/touch" prefix="owo" %>
<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--Declare page as mobile friendly --> 
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
		<!-- Declare page as iDevice WebApp friendly -->
		<meta name="apple-mobile-web-app-capable" content="yes"/>
		<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/splash/splash-icon.png">

		<!-- Page Title -->
		<title></title>

		<!--Page Scripts Load -->
		<script src="/resources/scripts/jquery.js"		 type="text/javascript"></script>

		<!-- Stylesheet Load -->
		<link href="/resources/styles/hardtouch.css" rel="stylesheet" type="text/css">
	</head>
	<body>	
		<owo:headerSprite />
		<h1 class="center-text highlight bg-gray uppercase">Ooops, lo sentimos</h1>
		<div class="paycontainer">
			<p>Lo sentimos, se ha producido un error en el sistema, puedes volver a intentarlo pasados unos minutos.</p>
		</div>
	</body>
</html>
