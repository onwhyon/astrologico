<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/html5" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<%@ include file="../inc/headHtml5.jsp" %>
	<body>
		<owo:headerMenuSprite list="${headermenucats }" menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />
			<p style="text-align: center;">${toptext }</p>

			<owo:title1row title="${title1row_0_002 }" />
			<owo:mainSlider list="${mainslider_1 }" pag="<%=PRM + CNT %>" />
			<owo:article1rowCnts list="${article2colos_1 }" pag="<%=PRM + CNT %>" />
		</div>
		<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
	</body>
</html>
