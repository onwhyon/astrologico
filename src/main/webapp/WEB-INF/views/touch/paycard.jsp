﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/html5" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../inc/ctes.jsp"%>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ include file="../inc/headHtml5.jsp" %>

<body class="body-class">

	<owo:header image="${headermenu_0_106 }"
		imageAlt="${headermenu_0_016 }" />

		<div class="container botones">

		<p class="center-text" style="margin-top: 80px">
			Astrologico<br><br>
			Suscripción: 7 días renovable<br>
			Precio: 1,21 (IVA incl.)<br>
		</p><br>

		<div class="center-text">
					<div class="btn">
						<a href="${subscribe}">Comprar</a>
					</div><br><br>

				<p class="left-text">
					Si clicas en Comprar aceptas las <a href="${pageContext.request.contextPath}/condicionesservicio.htm?return=${return }">Condiciones legales</a>
							<br><br>
					<a href="${pageContext.request.contextPath}/index.htm">Cancelar</a>	
				</p>
		</div>
	</div>
</body>
</html>