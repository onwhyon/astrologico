<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/html5" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headHtml5.jsp" %>
	<body>
		<owo:headerMenuSprite list="${headermenucats }" menuTitle="${headermenu_0_002 }" pag="<%=CNTS %>" />
			<owo:title1row title="${title1row_1_002 }"/>
			<owo:banner rect="${banner_1_012 }" image="${banner_1_101 }" imgalt="${banner_1_016 }" />
			<owo:introNews text="${intronews_1_005 }" />
			<br>
			<owo:taskList list="${tasklist_1_008 }" title="${tasklist_1_007 }" />
			<br>
			<owo:bodyNews text="${bodynews_1_006 }" />
			<br>

		</div>
	</body>
</html>