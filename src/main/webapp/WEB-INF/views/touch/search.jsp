﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/touch" prefix="owo" %>
<%@ include file="../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<%@ include file="../inc/headHtml5.jsp" %>
	<body>
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }" list="${headermenucats }" menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />
			<p style="text-align: center;"><span>${toptext }</span></p>
			<owo:title1row title="${title1row_0_002 }" />
			<div class="container"><p>${searcherrtxt }</p></div>
			<owo:article1rowB list="${article2col_1 }" pag="<%=PRM + CNT %>" />
			<div class="container"><p>Realizar otra búsqueda</p></div>
			<owo:search placeholder="Busca recetas" submitlabel="Buscar" />
			
		</div>
		<owo:ga url="<%=GA_URL %>" code="<%=GA_CODE_VDF %>" />
	</body>
</html>