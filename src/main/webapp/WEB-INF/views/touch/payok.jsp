﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/html5" prefix="owo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="../inc/ctes.jsp"%>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ include file="../inc/headHtml5.jsp"%>
<body class="body-class">
	<owo:header image="${headermenu_0_106 }"
		imageAlt="${headermenu_0_016 }" />
		<div class="container botones">
			<p class="center-text" style="margin-top: 80px">
				Suscripción realizada con éxito. 
			</p>
			<p class="center-text">
				Coste del servicio después de la promoción 1,21 € /semana (IVA incl.)
			</p>
			<p style="text-align: center;">
				<a href="${pageContext.request.contextPath}/${link}"
					class="button-minimal grey-minimal">ACCEDER AL CONTENIDO</a>
			</p>
	        <p class="center-text">
				Puedes consultar las 
				<a href="${pageContext.request.contextPath}/condicionesservicio.htm?return=${link }">condiciones legales aquí</a>
						<br>
			</p>
		</div>
</body>
</html>