<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/legacy" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<%@ include file="../inc/headPml.jsp" %>
	<body>
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"/>
		<owo:menu list="${headermenucats }" pag="<%=PRM + CAT %>"/>

		<owo:title1row title="${title1row_0_002 }" />
		<owo:banner rect="${banner_1_012_1 }" image="${banner_1_101_1 }" imgalt="${banner_1_016_1 }" 
		title="${banner_1_002_1 }" link="${banner_1_015_1 }" friendurl="${banner_1_020_1 }" pag="<%=PRM + CNT %>"/>

		<owo:article2ColOS list="${article2colos_1 }" maxelems="4" pag="<%=PRM + CNT %>" />
		<owo:title1row title="TUS SECCIONES" />
		<owo:article1row list="${article2col_0}"  lnkall="${article2col_0_015}" pag="<%=CNTS %>"/>

	</body>
</html>