﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/legacy" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headPml.jsp" %>
	<body>	
		
		<body>	

		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }"  />

    	<table style="text-align: left;">
	    		<tr>
		    		<td>
		 <c:if test="${gestion=='false'}">
			<p ALIGN="CENTER">
				<strong>Condiciones particulares del Servicio</strong>
			</p>

			<p ALIGN="CENTER">
				<BR>
			</p>
			<p ALIGN="JUSTIFY">Vodafone España, con domicilio social en
				Avenida de América, 115, 28042 Madrid, informa al cliente que para
				el disfrute de los distintos contenidos prestados a través de
				Vodafone, puede optar, en algunos casos por el pago único,
				suscripciones o bonos, siempre contra su saldo o factura Vodafone.</p>
			<p ALIGN="JUSTIFY">Para formalizar la contratación solicitada,
				sólo tiene que pulsar el botón “CONFIRMAR COMPRA” que encontrará en
				cada página de cobro, aceptación que será archivada por Vodafone.</p>
			<p ALIGN="JUSTIFY">Vodafone no responderá ni por daños directos,
				ni indirectos, ni por el daño emergente, ni por el lucro cesante,
				por los eventuales perjuicios derivados del uso de la información y
				contenidos accesibles o contenidos de terceros a través de las
				conexiones y accesos existentes</p>
			<p ALIGN="JUSTIFY">Asimismo, Vodafone no será en ningún caso
				responsable, ni siquiera de forma directa o subsidiaria de productos
				o servicios prestados u ofertados por otras personas o entidades, o
				por contenidos, informaciones, comunicaciones, opiniones o
				manifestaciones de cualquier tipo originados o vertidos por terceros
				y que resulten accesibles a través de las páginas de los portales
				que ofrece Vodafone.</p><br>

			<p ALIGN="JUSTIFY"><a href="${pageContext.request.contextPath}/<%= request.getParameter("return") %>">Volver</a></p>

</c:if>
<c:if test="${gestion=='true'}">

</c:if>
		            	</td>
	            	</tr>
    		</table>
		
	</body>
</html>