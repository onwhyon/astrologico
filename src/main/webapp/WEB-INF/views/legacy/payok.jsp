﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/legacy" prefix="owo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="../inc/ctes.jsp"%>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ include file="../inc/headPml.jsp"%>
<body>
	<owo:headerMenu image="${headermenu_0_106 }"
		imageAlt="${headermenu_0_016 }" />

	<table class="container">
		<tr>
			<td>
			<p style="text-align: center; margin-top: 12px;" class="mod-intro">
				Suscripción realizada con éxito.
			</p>
			<p style="text-align: center; margin-top: 12px;" class="mod-intro">
				Coste del servicio 1,21 € /semana (IVA incl.)
			</p>
			</td>
		</tr>
		<tr>
			<td>
				<h2 style="text-align: center;">
					<a href="${pageContext.request.contextPath}/${link} "
						class="botonGrey">ACCEDER AL CONTENIDO</a>
				</h2>
			</td>
		</tr>
		<tr style="text-align: left;">
			<td>
				<p>
				Puedes consultar las 
				<a href="${pageContext.request.contextPath}/condicionesservicio.htm?return=${link }">condiciones legales aquí</a>
						<br>
				</p>
			</td>
		</tr>
	</table>
</body>
</html>