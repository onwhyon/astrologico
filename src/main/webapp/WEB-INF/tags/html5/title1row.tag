<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="title" description="Texto que irá en h1 y centrado" %>

<h1 class="center-text highlight bg-gray uppercase">${title }</h1>