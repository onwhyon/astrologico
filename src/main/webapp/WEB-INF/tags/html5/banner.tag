<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Muestra un banner con un texto debajo si se le pasa como atributo, el atributo rect=true indica si se quiere el banner con un borde, formato vertical: banner, [texto]" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://example.com/functions" prefix="f" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="image" description="Banner que se va a mostrar" %>
<%@ attribute name="imgalt" description="Alt del banner" %>
<%@ attribute name="text" description="texto debajo del banner" %>
<%@ attribute name="title" description="texto debajo del banner" %>
<%@ attribute name="rect" required="true"
	description="Hay dos tipos de banner uno con recuadro y otro sin el este booleano determina cual se dibuja" %>

<%@ attribute name="link" %>
<%@ attribute name="friendurl" description="" %>
<%@ attribute name="pag" %>
<%@ attribute name="site" %>

	<c:if test="${fn:length(link)!=0 }"><c:set var="itemlink" value="premium/${link}" /></c:if>
	<c:if test="${fn:length(friendurl)!=0 }"><c:set var="itemlink" value="${pag }/${friendurl}" /></c:if>
	<c:set var="vdflink" value="http://mw-vidasaludable.onwhyon.com/${itemlink }" />

<c:if test="${image != null && fn:length(image) > 0 }">
	<c:if test="${fn:length(title) > 0 }">
		<h4 class="heading center-text title-mod">${title }</h4>
	</c:if>
	<c:choose>
		<c:when test="${rect }">
			<div class="container banner-rect">
				<c:if test="${fn:length(itemlink) > 0 }">
					<a href="//live.vodafone.com/cae/main.do?scn=ec&pid=1244&sid=0124400366&xpath=${f:urlEnconder(vdflink)}">
				</c:if>
						<img src="<%=IMGS_PATH %>${image }" alt="${imgalt }" class="banner_rect-mod">
				<c:if test="${fn:length(itemlink) > 0 }">
					</a>
				</c:if>
				<p>${text }</p>
			</div>
		</c:when>
		<c:otherwise>
			<div class="banner-div-mod">
				<c:if test="${fn:length(itemlink) > 0 }">
					<a href="//live.vodafone.com/cae/main.do?scn=ec&pid=1244&sid=0124400366&xpath=${f:urlEnconder(vdflink)}">
				</c:if>
						<img src="<%=IMGS_PATH %>${image }" alt="${imgalt }" class="banner-mod">
				<c:if test="${fn:length(itemlink) > 0 }">
					</a>
				</c:if>
				<c:if test="${fn:length(text) > 0 }">
					<p>${text }</p>
				</c:if>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>