<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="title" description="Título del listado" %>
<%@ attribute name="list" type="java.util.Iterator" description="Iterador con los elementos a mostrar" %>
<%@ attribute name="pag" description="'Página' destino para las url friendly" %>
<div class="container">
 <h4 class="heading center-text">${title}</h4>
</div>
<div class="container-gray">
	<c:forEach items="${list }" var="item" varStatus="status" >
		 <c:set var="itemtitle" value="noencontrado tipo 2" />
		 <c:set var="itemttext" value="noencontrado tipo 4" />
		 <c:set var="itemlink" value="" />
		 <c:set var="itemimgalt" value="noencontrado tipo 16" />
		 <c:set var="itemimage" value="noencontrado img 6" />
		 <c:forEach items="${item.texts }" var="aux">
		  <c:choose>
		   	<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
		   	<c:when test="${aux.idTextType == 4 }"><c:set var="itemttext" value="${aux.text }" /></c:when>
		   	<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 16 }"><c:set var="itemimgalt" value="${aux.text }" /></c:when>
		  </c:choose>
		 </c:forEach>
		 <c:forEach items="${item.images }" var="aux">
		  <c:if test="${aux.idImageType == 3 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
		 </c:forEach>
		 
		 <c:if test="${status.first || (status.count%2==1) }">
		  <div class="column">
		 </c:if>
		 
		<c:set var="class" value="${!((status.count % 2 == 1) && status.last) ? 'one-half' : 'container' }" />
	 	  <div class="${class }"style="text-align:center;">
		  <a href="${pageContext.request.contextPath}/${itemlink }">
		   <img class="center-icon" src="${pageContext.request.contextPath}/resources/${itemimage }" alt="${itemimgalt }" />
		   <h4>${itemtitle }</h4>
		   <p class="no-bottom">${itemttext }</p> 
		  </a>
		 </div>
		 <!-- Segundo de la columna -->
		 <c:if test="${status.last || (status.count%2==0) }">
		  </div>
		 </c:if>
	</c:forEach>
</div>