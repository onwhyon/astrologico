﻿<%@ tag language="java" pageEncoding="UTF-8"
description="OJO!!!!!!! Si se usa este tag hay que cerrar el div: &lt;div id='content' class='page-content'&gt; al final de la página ya que si no los elementos no se encajan correctamente" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="image" description="Imágen del menu" %>
<%@ attribute name="imageAlt" description="Alt para las imágenes a mostrar" %>

<div class="payheader">
		<p class="logo">
			<img src="${pageContext.request.contextPath}/resources/${image }" alt="${imageAlt }">
		</p>
	</div>

