<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://example.com/functions" prefix="f" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="title" description="Título del listado" %>
<%@ attribute name="list" type="java.util.Iterator" description="Iterador con los elementos a mostrar" %>
<%@ attribute name="pag" description="'Página' destino para las url friendly" %>
<%@ attribute name="site" description="site" %>


<div class="container">
 <h4 class="heading center-text bg-light-gray">${title}</h4>
</div>

<c:forEach items="${list }" var="item" varStatus="status" >
 <c:set var="itemtitle" value="noencontrado tipo 17" />
 <c:set var="itemttext" value="noencontrado tipo 19" />
 <c:set var="itemlink" value="" />
 <c:set var="itemimgalt" value="noencontrado tipo 16" />
 <c:set var="itemimage" value="noencontrado img 2" />
 <c:forEach items="${item.texts }" var="aux">
  <c:choose>
   	<c:when test="${aux.idTextType == 17 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
   	<c:when test="${aux.idTextType == 19 }"><c:set var="itemttext" value="${aux.text }" /></c:when>
  	<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="premium/${aux.text }" /></c:when>
	<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
   	<c:when test="${aux.idTextType == 16 }"><c:set var="itemgimgalt" value="${aux.text }" /></c:when>
  </c:choose>
 </c:forEach>
 <c:forEach items="${item.images }" var="aux">
  <c:if test="${aux.idImageType == 2 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
 </c:forEach>
 <c:if test="${status.first || (status.count%2==1) }">
  <div class="column">
 </c:if>
 
<c:set var="class" value="${!((status.count % 2 == 1) && status.last) ? 'one-half' : 'container' }" />
<c:set var="vdflink" value="http://mw-vidasaludable.onwhyon.com/${itemlink }" />

 <div class="${class }"style="text-align:center;">
  	<a href="//live.vodafone.com/cae/main.do?scn=ec&pid=1244&sid=0124400366&xpath=${f:urlEnconder(vdflink)}">
   <h4>${itemtitle }</h4>
   <img src="<%=IMGS_PATH %>${itemimage }" width="125" height="70" alt="${itemimgalt }" >
   <p class="no-bottom">${itemttext }</p> 
  </a>
 </div>
 <!-- Segundo de la columna -->
 <c:if test="${status.last || (status.count%2==0) }">
  </div>
 </c:if>
</c:forEach>