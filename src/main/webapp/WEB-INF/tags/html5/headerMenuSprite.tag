﻿<%@ tag language="java" pageEncoding="UTF-8"
description="OJO!!!!!!! Si se usa este tag hay que cerrar el div: &lt;div id='content' class='page-content'&gt; al final de la página ya que si no los elementos no se encajan correctamente" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="menuTitle" description="Título del menú" %>
<%@ attribute name="list" type="java.util.Iterator" description="Lista de las secciones (normálmente el listado de los contenidos de la categoría padre del portal)" %>
<%@ attribute name="pag" description="'Página' destino para las url friendly" %>

<div id="sidebar" class="page-sidebar">
	<div class="page-sidebar-scroll">
		<div class="sidebar-header">
			<a href="#" class="delete-sidebar"></a>
			<div class="logo-site-menu"></div>
		</div> 

		<p class="sidebar-separator">${menuTitle }</p>
		<div class="menu-row"><a href="/index.html"><div class="nav-item home-nav"></div>  INICIO     </a></div>
		<c:forEach items="${list }" var="item" varStatus="status">
			<c:set var="itemlink" value="" />
			<c:set var="itemtitle" value="" />
			<c:forEach items="${item.texts }" var="aux">
				<c:choose>
					<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
					<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
				</c:choose>
			</c:forEach>
			<c:forEach items="${item.images }" var="aux">
				<c:if test="${aux.idImageType == 5 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
			</c:forEach>
			<c:set var="icoClass" value="${(fn:length(itemimage)>0) ? itemimage : 'type-nav' }" />
			<div class="menu-row">
				<a href="${pageContext.request.contextPath}/${itemlink }">  
					<div class="nav-item ${icoClass } ${(status.last)?'no-border':''}"></div> ${itemtitle }
				</a>
			</div>
		</c:forEach>
	</div>
</div>

<div id="content" class="page-content">
	<div class="page-header">
		<div class="logo-header"></div>
		<a href="#" class="deploy-sidebar"></a>
	</div>