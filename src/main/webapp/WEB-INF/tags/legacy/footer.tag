﻿<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="fblnk" description="Enlace para condiciones"%>
<%@ attribute name="twlnk" description="Enlace para suscripcion"%>

<table style="text-align: center;" class="footer">

	<tr>
		<td><a
			href="${pageContext.request.contextPath}/privacidad.htm"
			class="enlace">Privacidad</a></td>
		<td><a
			href="${pageContext.request.contextPath}/terminos.htm"
			class="enlace">Condiciones</a></td>
	</tr>
</table>
