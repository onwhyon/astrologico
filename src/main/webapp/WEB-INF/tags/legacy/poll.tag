<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="content" type="com.owo.dao.domain.content.Content" description="Textos del objeto sobre el que se hace la encuesta" %>


<script>
 function objetoAjax(){
  var xmlhttp=false;
  try {
   xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
   try {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
   } catch (e) {
    xmlhttp = false;
   }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
   xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
 }
 
 function MostrarConsulta(datos){
  divResultado = document.getElementById('barra');
  ajax=objetoAjax();
  ajax.open("GET", datos);
  ajax.onreadystatechange=function() {
   if (ajax.readyState==4) {
    divResultado.classList.remove("p10");
    divResultado.classList.add(datos);
    //divResultado.className += datos;//ajax.responseText;
   }
  }
  ajax.send(null);
 }
</script>

<c:set var="itemtitle" value="" />
<c:set var="itemtext" value="" />
<c:set var="itemko" value="" />
<c:set var="itemok" value="" />
<c:set var="itemresults" value="" />
<c:set var="itemimgalt" value="" />
<c:set var="itemimage" value="" />

<c:forEach items="${content.texts }" var="aux">
 <c:choose>
  <c:when test="${aux.idTextType == 2}"><c:set var="itemtitle" value="${aux.text }" /></c:when>
  <c:when test="${aux.idTextType == 4}"><c:set var="itemtext" value="${aux.text }" /></c:when>
  <c:when test="${aux.idTextType == 12}"><c:set var="itemko" value="${aux.text }" /></c:when>
  <c:when test="${aux.idTextType == 13}"><c:set var="itemok" value="${aux.text }" /></c:when>
  <c:when test="${aux.idTextType == 14}"><c:set var="itemresults" value="${aux.text }" /></c:when>
  <c:when test="${aux.idTextType == 16}"><c:set var="itemimgalt" value="${aux.text }" /></c:when>
 </c:choose>
</c:forEach>
<c:forEach items="${content.images }" var="aux">
 <c:if test="${aux.idImageType == 4 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
</c:forEach>

<table style="text-align: center; ">
 <tr >
  <td colspan="2" >
  <h2 ><span class="encuesta">${itemtitle}</span></h2>
  </td>
 </tr>
 <tr>
  <td colspan="2">
  <h2>${itemtext }</h2>
  </td>
 </tr>
 <tr>
 <td colspan="2">
 <h4 >${imgname }</h4>
  <img src="${pageContext.request.contextPath}/resources/${itemimage }" alt="${itemimgalt }" class="tableColumn" />
</td>
 </tr>
 <tr>
  
   <form action="poll.htm" method="post">
   
   <td class="tableColumn">
     <input type="hidden" name="idContent" value="${content.idContent }"/>
     <input class="botonGrey" type="submit" name="ko" value="${itemko }" >
     </td>
     
     <td class="tableColumn">
     <input class="botonColor" type="submit" name="ok" value="${itemok }" >
     </td>
     
   </form>
  </td>
 </tr>
 <tr>
  <td colspan="2">
   <h4 class="boldTitles">${itemresults}</h4>
  </td>
 </tr>
 <tr>
  <td class="tableColumn">
  <p ><strong >${itemko }</strong></p>
  </td>
  <td class="tableColumn">
   <p class="textColor"><strong ><b>${itemok }</b></strong></p>
  </td>
 </tr>
 <tr style="background-color:#e6e6e6;">
  <td colspan="2" >
   <span >
       <span id="barra" class="percent red-minimal ${pollbar }"></span>
   </span>
  </td>
 </tr>
 </table>