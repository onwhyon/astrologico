﻿<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="pag" %>
<%@ attribute name="title" description="Encabezado" %>
<%@ attribute name="list" type="java.util.Iterator" description="Listado de noticias, 1 columna con imagen" %>
<%@ attribute name="maxelems" type="java.lang.Integer" description="Número máximo de elementos a mostrar, por defecto 4" %>
<%@ attribute name="seeaell" description="Texto del enlace ver todas" %>
<%@ attribute name="lnkall" description="Enlace para ver todas" %>

<c:if test="${fn:length(title)!=0 }">
	<table style="text-align: center;">
	 <tr>
	  <td class="encuesta">
   		 <h2 >${title }</h2>
	   </td>
	  </tr>
	</table>
</c:if>

<c:set var="maxelems" value="${(maxelems>0)? maxelems : 4 }" />
<c:set var="itemtitle" value="noencontrado tipo 2" />
<c:set var="itemttext" value="noencontrado tipo 4" />
<c:set var="itemlink" value="" />
<c:set var="itemgimgalt" value="noencontrado tipo 16" />
<c:set var="itemimage" value="noencontrado img 6" />

<table class="art1col">

 <c:forEach items="${list }" var="item" end="${maxelems }" >
  <c:forEach items="${item.texts }" var="aux">
	   <c:choose>
	    <c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
	    <c:when test="${aux.idTextType == 4 }"><c:set var="itemttext" value="${aux.text }" /></c:when>
	    <c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
	    <c:when test="${aux.idTextType == 16 }"><c:set var="itemgimgalt" value="${aux.text }" /></c:when>
	    <c:when test="${aux.idTextType == 15 and (fn:length(itemlink)==0)}"><c:set var="itemlink" value="${aux.text }" /></c:when>
	   </c:choose>

  </c:forEach>
	<c:forEach items="${item.images }" var="aux">
		<c:if test="${aux.idImageType == 6 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
	</c:forEach>
  
  <tr >
     <td class="tableThird" >
     
     <a href="${pageContext.request.contextPath}/${itemlink }">
     <img src="${pageContext.request.contextPath}/resources/${itemimage }"  alt="${itemgimgalt }" class="tableExpand">
     </a>
     </td>
    
   <td style="text-align: left;" class="tableColum">
    <a href="${pageContext.request.contextPath}/${itemlink }">
    	 <h4 class="boldTitles"><b>${itemtitle }</b></h4>
     </a>
     <p > ${ itemttext} </p>  
   </td>

  </tr>
<c:set var="itemlink" value="" />
 </c:forEach>
     
</table>