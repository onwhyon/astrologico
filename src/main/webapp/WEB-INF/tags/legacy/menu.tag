<%@ tag language="java" pageEncoding="UTF-8"
description=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="list" type="java.util.Iterator" description="Listado de los elementos a mostrar" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="pag" %>

<table>
 <tr>
   <td  style="text-align:center;">  
 <span class="titles"> | </span>
      <a href="${pageContext.request.contextPath}/index.htm"><span class="titles"> Inicio </span></a>
 <span class="titles"> | </span>
    <c:forEach items="${list }" var="item" varStatus="status" >
     <c:set var="itemtitle" value="" />
     <c:set var="itemtext" value="" />
     <c:set var="itemlink" value="" />
     <c:forEach items="${item.texts }" var="aux">
      <c:choose>
       <c:when test="${aux.idTextType == 2}"><c:set var="itemtitle" value="${aux.text }" /></c:when>
       <c:when test="${aux.idTextType == 20}"><c:set var="itemlink" value="${pag}/${aux.text }" /></c:when>
       <c:when test="${aux.idTextType == 15 and (fn:length(itemlink)==0)}"><c:set var="itemlink" value="${aux.text }" /></c:when>
      </c:choose>
      
     </c:forEach>
      <a href="${pageContext.request.contextPath}/${itemlink }">

  
        <span class="titles">  ${itemtitle } </span>
        </a>
  <span class="titles"> | </span>
  <c:set var="itemlink" value="" />
    </c:forEach>
    </td>
   </tr>
</table>