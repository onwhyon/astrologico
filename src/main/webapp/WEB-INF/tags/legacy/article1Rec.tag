<%@ tag language="java" pageEncoding="UTF-8" description="Usa los mismos datos que article3colonlytext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="title" %>
<%@ attribute name="list" type="java.util.Iterator" %>

<table>

	<c:forEach items="${list }" var="item" varStatus="status">
		<c:if test="${status.first }">	
				<c:if test="${fn:length(title)>0 }">
					<tr>
						<td>
			       	       <h4 class="boldTitles">${title }</h4>	
				       </td>	
				    </tr>
			</c:if>
			
		</c:if>
		<c:set var="itemtitle" value="noencontrado tipo 2" />
		<c:set var="itemlink" value="" />
	
		<c:forEach items="${item.texts }" var="aux">
			<c:choose>
				<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
				<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
			</c:choose>
				<c:if test="${(fn:length(itemlink)==0) and (aux.idTextType == 15)}">
    					<c:set var="itemlink" value="${aux.text }" /> 
   				 </c:if>
		</c:forEach>
		
		<c:if test="${status.count % 2 == 0}"><c:set var="class" value="greenline" /></c:if>
		<c:if test="${status.count % 2 == 1}"><c:set var="class" value="darkgreen" /></c:if>
		
		<tr class="${class }">
			<td>
				<a href="//${itemlink }"><span class="tableExpand">${itemtitle } </span></a>
			</td>
		</tr>

	</c:forEach>
</table>