<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="placeholder" description="Placeholder a mostrar" %>
<%@ attribute name="submitlabel" description="Etiqueta del boton buscar" %>

 <table class="tableSearch" style="text-align: center; ">
	<tr >
		<td >
			<form action="search.htm" method="post" accept-charset="ISO-8859-1" >
        	<input type="text" placeholder="${placeholder }" required style="width: 75%" id="txt" name="txt">
       		 <button class="botonColor" type="submit">${submitlabel }</button>
   			 </form>
		</td>
	</tr>
</table>
