<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Muestra un encabezado, seguido de una lista de elementos extraidos de una iterador" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="title" description="Texto del encabezado de la lista" %>
<%@ attribute name="list" description="Elementos que se mostrarán en el UL" %>
<c:if test="${list!=null }">
<table>
 

	<c:forTokens items="${list }" delims="#" var="item">
		<c:choose>
			<c:when test="${!fn:contains(item, '|') }">
			 <tr>
			 <td>
				<h4>${item }</h4>
				</td>
				</tr>
			</c:when>
			<c:when test="${fn:contains(item, '|') }">
			<tr>
 				<td>
 				<c:if test="${fn:length(title) > 0 }">
		<h4 >${title }</h4>
	</c:if>
				<ul>
					<c:forTokens items="${item }" delims="|" var="itemlist">
						<li>${itemlist }</li>
					</c:forTokens>
				</ul>
				</td>
  			</tr>
			</c:when>
		</c:choose>
	</c:forTokens>
</table>
</c:if>