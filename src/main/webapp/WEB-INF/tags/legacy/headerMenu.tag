<%@ tag language="java" pageEncoding="UTF-8"
	description="OJO!!!!!!! Si se usa este tag hay que cerrar el div: &lt;div id='content' class='page-content'> al final de la página ya que si no los elementos no se encajan correctamente"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="image" description="Imágen del menu"%>
<%@ attribute name="imageAlt"
	description="Alt para las imágenes a mostrar"%>


<table class="header1">
	<tr>
		<td style="text-align: center;"><a
			href="${pageContext.request.contextPath}/index.htm"> <img
				src="${pageContext.request.contextPath}/resources/${image }"
				alt="${imageAlt } " style="width: 60%;" />
		</a></td>
	</tr>
</table>