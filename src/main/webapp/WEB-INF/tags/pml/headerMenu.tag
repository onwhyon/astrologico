<%@ tag language="java" pageEncoding="UTF-8" %>


<%@ attribute name="image" description="Imágen del menu" %>
<%@ attribute name="imageAlt" description="Alt para las imágenes a mostrar" %>


<table>
	<tr >
		<td  style="text-align:center;">
		<a href="${pageContext.request.contextPath}/index.htm">
	  	 <img src="${pageContext.request.contextPath}/resources/images/logopml.jpg" alt="${imageAlt } "  style="width: 100%;"/>
	 	</a>
	  </td>
	  </tr>
</table>