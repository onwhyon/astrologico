<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag
	description="Muestra un banner con un texto debajo si se le pasa como atributo, el atributo rect=true indica si se quiere el banner con un borde, formato vertical: banner, [texto]"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://example.com/functions" prefix="f" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="title" description="titulo encima del banner"%>
<%@ attribute name="image" description="Banner que se va a mostrar"%>
<%@ attribute name="imgalt" description="Alt del banner"%>
<%@ attribute name="text" description="texto debajo del banner"%>
<%@ attribute name="link" description="link del banner"%>
<%@ attribute name="friendurl" description="url amigable del banner"%>
<%@ attribute name="pag" %>
<%@ attribute name="site" %>

<%@ attribute name="rect" required="true"
	description="Hay dos tipos de banner uno con recuadro y otro sin el, este booleano determina cual se dibuja"%>

<c:if test="${fn:length(title)!=0 }">
	<table style="text-align: center;">
		<tr>
			<td>
				<h4 class="titles">${title}</h4>
			</td>
		</tr>
	</table>
</c:if>

	<c:if test="${fn:length(link)!=0 }"><c:set var="link" value="premium/${link}" /></c:if>
	<c:if test="${fn:length(friendurl)!=0 }"><c:set var="link" value="${pag }/${friendurl}" /></c:if>
	<c:set var="vdflink" value="http://vl-vidasaludable.onwhyon.com/${link }" />
<c:if test="${image != null && fn:length(image) > 0 }">
	<c:choose>
		<c:when test="${rect }">
			<table>
				<tr>
					<td class="tableExpand"><c:choose>
							<c:when test="${fn:length(link)>0 }">
								 <a href="//live.vodafone.com/cae/main.do?scn=ec&pid=1244&sid=0124400366&xpath=${f:urlEnconder(vdflink)}"> <img
									src="<%=IMGS_PATH %>${image }"
									alt="${imgalt }" class="tableExpand">
								</a>
							</c:when>
							<c:otherwise>
								<img
									src="<%=IMGS_PATH %>${image }"
									alt="${imgalt }" class="tableExpand">
							</c:otherwise>
						</c:choose>
						<p>${text }</p></td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<table>
				<tr>
					<td class="tableExpand"><c:choose>
							<c:when test="${fn:length(link)>0 }">
								 <a href="//live.vodafone.com/cae/main.do?scn=ec&pid=1244&sid=0124400366&xpath=${f:urlEnconder(vdflink)}"> <img
									src="<%=IMGS_PATH %>${image }"
									alt="${imgalt }" class="tableExpand">
								</a>
							</c:when>
							<c:otherwise>
								<img
									src="<%=IMGS_PATH %>${image }"
									alt="${imgalt }" class="tableExpand">
							</c:otherwise>
						</c:choose>
						<p>${text }</p></td>
				</tr>
			</table>
		</c:otherwise>
	</c:choose>
</c:if>