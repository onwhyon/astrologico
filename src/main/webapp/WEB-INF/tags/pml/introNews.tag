<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Texto corto de introducción" %>

<%@ attribute name="text" description="texto a mostrar" %>
<table >
	<tr>
		<td>
			<p >${text }</p>
		</td>
	</tr>
</table>
