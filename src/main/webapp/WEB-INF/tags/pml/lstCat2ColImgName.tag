<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Listado a dos columnas mostrando por elemento y de arriba-abajo: Icono, texto" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="title" description="Título del listado" %>
<%@ attribute name="list" type="java.util.Iterator" description="Listado de categorías" %>
<%@ attribute name="lnkmore" description="Link ver más" %>
<%@ attribute name="seemore" description="Texto del link ver más" %>

<c:set var="maxelems" value="${maxelems > 0 ? maxelems : 6 }" />
<table >
<tr>
<td>
	<h4 class="heading center-text bg-gray">${title}</h4>
	</td>
	</tr>
</table>

<table >
<tr >

		<c:forEach items="${list }" var="item" varStatus="status" end="${ maxelems}" >
			<c:set var="itemlink" value="" />
			<c:set var="itemtext" value="" />
			<c:set var="itemtitle" value="" />
			<c:set var="itemalt" value="" />
			<c:forEach items="${item.texts }" var="aux">
				<c:choose>
					<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 4 }"><c:set var="itemtext" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 16 }"><c:set var="itemalt" value="${aux.text }" /></c:when>
				</c:choose>
			</c:forEach>
			<c:forEach items="${item.images }" var="aux">
				<c:if test="${aux.idImageType == 3 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
			</c:forEach>
			
			
			<c:set var="class" value="${!((status.count % 2 == 1) && status.last) ? 'tableColumn' : 'tableExpand' }" />
			
			<c:choose>
					<c:when test="${class=='tableColumn'}">
				<td class="${class }">
				</c:when>
				<c:otherwise>
					<td class="${class }" colspan="2">
				</c:otherwise>
				
			</c:choose>
			
			
				<a href="${itemlink }"><img class="center-icon" src="${itemimage }" alt="${itemalt }"></a>
				<h5 class="center-text uppercase">${itemtitle}</h5>
				<p class="center-text no-bottom">
				   ${itemtext }<br>
				</p>
			
			<c:choose>
					<c:when test="${(status.count % 2 == 0)&&!status.last&&!status.first}">
				</td></tr><tr>
				</c:when>
				<c:otherwise>
					</td>
				</c:otherwise>
				
			</c:choose>
			
		</c:forEach>
		
	</tr>
</table>
