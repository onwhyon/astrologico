<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="content" type="com.owo.dao.domain.content.Content" description="Textos del objeto sobre el que se hace la encuesta" %>

<c:set var="itemtitle" value="" />
<c:set var="itemtext" value="" />
<c:set var="itemko" value="" />
<c:set var="itemok" value="" />
<c:set var="itemresults" value="" />
<c:set var="itemimgalt" value="" />
<c:set var="itemimage" value="" />

<c:forEach items="${content.texts }" var="aux">
	<c:choose>
		<c:when test="${aux.idTextType == 2}"><c:set var="itemtitle" value="${aux.text }" /></c:when>
		<c:when test="${aux.idTextType == 4}"><c:set var="itemtext" value="${aux.text }" /></c:when>
		<c:when test="${aux.idTextType == 12}"><c:set var="itemko" value="${aux.text }" /></c:when>
		<c:when test="${aux.idTextType == 13}"><c:set var="itemok" value="${aux.text }" /></c:when>
		<c:when test="${aux.idTextType == 14}"><c:set var="itemresults" value="${aux.text }" /></c:when>
		<c:when test="${aux.idTextType == 16}"><c:set var="itemimgalt" value="${aux.text }" /></c:when>
	</c:choose>
</c:forEach>
<c:forEach items="${content.images }" var="aux">
	<c:if test="${aux.idImageType == 4 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
</c:forEach>
<div class="column">
	<h2 class="center-text"><span class="highlight bg-gray poll-title-mod">${itemtitle}:</span><br/>${itemtext }</h2>

	<div  class="encuesta-imgdiv-mod">
		<img src="${pageContext.request.contextPath}/resources/${itemimage }" alt="${itemimgalt }" class="encuesta-img-mod" />
		<h4 class="heading center-text" class="encuesta-imgdiv-h4-mod">${imgname }</h4>
		<div class="encuesta-butt-mod">
			<form action="${pageContext.request.contextPath}/poll.htm" method="post">
					<input type="hidden" name="idContent" value="${content.idContent }"/>
					<input type="submit" name="ko" value="${itemko }" class="no-bottom  button-minimal grey-minimal" >
					<input type="submit" name="ok" value="${itemok }" class="no-bottom  button-minimal red-minimal" >
			</form>
		</div>
		<div class="encuesta-results-mod">
			<h4 class="heading center-text" class="encuesta-heading-mod">${itemresults}</h4>
			<div class="stat">
				<p class="stat-left">${itemko }</p>
				<p class="stat-right"><strong class="decrease"><b>${itemok }</b></strong></p>
				<div class="clear"></div>
				<span class="stat-background">
				    <span class="stat-cleaner"></span>
				    <span id="barra" class="percent red-minimal ${pollbar }"></span>
				</span>
			</div>
		</div>
	</div>
</div>