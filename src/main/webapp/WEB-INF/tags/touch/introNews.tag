<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Texto corto de introducción" %>

<%@ attribute name="text" description="texto a mostrar" %>
<div class="container">
	<p class="news-short-text-mod">${text }</p>
</div>
