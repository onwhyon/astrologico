<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="list" required="true" type="java.util.Iterator" %>
<%@ attribute name="pag" description="'P�gina' destino para las url friendly" %>

<div class="bxslider-wrapper" data-snap-ignore="true">
	<div class="bxslider">
		<c:forEach items="${list }" var="item">
			<c:set var="itemtitle" value="" />
			<c:set var="itemtext" value="" />
			<c:set var="itemlink" value="" />
			
			<c:forEach items="${item.texts }" var="aux">
				<c:choose>
					<c:when test="${aux.idTextType == 19 }"><c:set var="itemtext" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 16 }"><c:set var="itemimgalt" value="${aux.text }" /></c:when>
					<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
				</c:choose>
			</c:forEach>
			<c:set var="itemimage" value="" />
			<c:forEach items="${item.images }" var="aux">
				<c:if test="${aux.idImageType == 1 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
			</c:forEach>
			<a href="${pageContext.request.contextPath}/${itemlink }">
				<div>
					<img src="<%=IMGS_PATH %>${itemimage}" alt="${itemimgalt }"/>
					<blockquote class="slider-caption">
						<p class="slider-title slider-title-mod uppercase">
							<b>${itemtext }</b>
						</p>
					</blockquote>
				</div>
			</a>
		</c:forEach>
	</div>
</div>
