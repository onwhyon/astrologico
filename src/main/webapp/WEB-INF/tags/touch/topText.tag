<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="title" %>
<%@ attribute name ="text" %>

<div class="container">
	<h4 class="heading center-text title-mod">${title }</h4>
	<p class="center-text">${text }</p>
</div>