<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="placeholder" description="Placeholder a mostrar" %>
<%@ attribute name="submitlabel" description="Etiqueta del boton buscar" %>
<%--Buscador --%>

<c:url value="/search.htm" var="url" />
<form action="${url }" class="search-wrapper cf" method="post" accept-charset="ISO-8859-1" >
	<input type="text" placeholder="${placeholder }" required id="txt" name="txt">
	<button type="submit">${submitlabel }</button>
</form>
<div class="decoration"></div>