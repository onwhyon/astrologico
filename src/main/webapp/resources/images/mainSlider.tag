<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="list" required="true" type="java.util.Iterator" %>

<div class="bxslider-wrapper" data-snap-ignore="true">
	<div class="bxslider">
		<c:forEach items="${list }" var="item">
			<c:set var="itemtitle" value="" />
			<c:set var="itemtext" value="" />
			<c:set var="itemlink" value="" />
			
			<c:forEach items="${item.texts }" var="aux">
				<c:choose>
					<c:when test="${aux.idTextType == 19 }"><c:set var="itemtext" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 16 }"><c:set var="itemimgalt" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 1 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
				</c:choose>
			</c:forEach>
			<c:set var="itemimage" value="" />
			<c:forEach items="${item.images }" var="aux">
				<c:if test="${aux.idImageType == 1 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
			</c:forEach>
			<a href="${itemlink }">
				<div>
					<img src="${pageContext.request.contextPath}/resources/${itemimage}" alt="${itemimgalt }"/>
					<blockquote class="slider-caption">
						<p class="slider-title slider-title-mod uppercase">
							<b>${itemtext }</b>
						</p>
					</blockquote>
				</div>
			</a>
		</c:forEach>
	</div>
</div>
